/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import Animacion.EscenaBienvenida;
import Score.Scores;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 *Pantalla donde se ingresa el nombre del jugador para posteriormente pasar a guardar su score
 * @author GaryBarzola
 */
public class inDataName {
    private final StackPane root;
    private final VBox contenido;
    private TextField nombre;
    private ventanaPuntaje vP;
    private final double puntaje;
    
    public inDataName(double puntaje) {
        root = new StackPane();
        contenido = new VBox();
        pantalla();
        contenido();
        this.puntaje=puntaje;
    }
    

    public final void pantalla(){
        
        try {
            Image image = new Image(new FileInputStream(RECURSOS.CONSTANTES.PATH_IMAGES+"fondo_btn.jpg"));
            ImageView imagen1 = new ImageView(image);
            imagen1.setFitHeight(700);
            imagen1.setFitWidth(1000);
            root.getChildren().add(imagen1);
        } catch (FileNotFoundException ex) {
            System.out.println("Imagen de fondo no encontrada");
        }
    }
    
    public final void contenido(){
        Button guardar= new Button("Guardar");
        nombre= new TextField();
        Inicial.tipoLetra(nombre);
        Inicial.tipoLetra(guardar);
        Inicial.fondoBotones(guardar);
        Font theFont = Font.font("Helvetica", FontWeight.BOLD, 38 );
        Label l1= new Label("INGRESE NOMBRE DEL JUGADOR") ; l1.setTextFill(Color.web("#FFFFFF"));l1.setFont(theFont);
        contenido.getChildren().addAll(l1,nombre,guardar);
        contenido.setAlignment(Pos.CENTER);
        root.getChildren().add(contenido);
        guardar.setOnMouseClicked((e)->{
            vP= new ventanaPuntaje(nombre.getText(),puntaje);
            PaneTop.leerDataTop(new Scores(nombre.getText(),puntaje));  
            EscenaBienvenida.sc.setRoot(vP.getRoot());
                        
            });
    }

    public StackPane getRoot() {
        return root;
    }
    
}
