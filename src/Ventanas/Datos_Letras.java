/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import Animacion.EscenaBienvenida;
import java.io.FileInputStream;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import RECURSOS.CONSTANTES;
import java.io.FileNotFoundException;
import java.util.Map;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
/**
 *
 * @author Andrea
 */
public class Datos_Letras {
    private final Pane root;
    private Button continuar;
    public static Map<String, Integer> tuplaLetra; //recibe la tupla del PaneJuego
    private playPalabras pP;
    
    public Datos_Letras(){
        root= new Pane();
        tuplaLetra= PaneJuego.letrasPresionadas;
        crearFondo();
        boton_Continuar();
        crearInfo();
    }

    /**
     * Metodo que me crear el fondo de la pantalla
     */
    public final void crearFondo(){
        try {
            Image img= new Image(new FileInputStream(CONSTANTES.PATH_FILES+"fondo_btn.jpg"));
            ImageView image= new ImageView(img);
            image.setFitHeight(700);
            image.setFitWidth(1000);
            root.getChildren().add(image);
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    /**
     * Metodo que me crear el boton continuar, para dar paso al juego de palabras
     */
    public final void boton_Continuar(){
        continuar= new Button("CONTINUAR");
        continuar.setPrefSize(160, 70);
        Inicial.tipoLetra(continuar);
        Inicial.fondoBotones(continuar);
        continuar.setLayoutX(430);
        continuar.setLayoutY(40);
        root.getChildren().add(continuar);
        continuar.setOnAction((e)->{
            pP= new playPalabras();
            EscenaBienvenida.sc.setRoot(pP.getRoot());
        });

    }
    
    public final void crearInfo(){
        //contarLetras();
        VBox datos= new VBox();
        FlowPane contenidoLetras= new FlowPane();
        Label r= new Label("RESULTADO");
        Inicial.tipoLetra(r);
        contenidoLetras.getChildren().addAll(recorrerLetra(tuplaLetra,15));
        contenidoLetras.setAlignment(Pos.CENTER);
        datos.getChildren().addAll(r,contenidoLetras);
        datos.setAlignment(Pos.CENTER);
        datos.setLayoutX(313);
        datos.setLayoutY(115);

        root.getChildren().addAll(datos);
    }
    
    /**
     * Metodo que me aniade el stock a la pantalla
     * @param mapa que se va a recorrer para leer sus datos
     * @param tamanioLetra para darle tamanio a la letra 
     * @return un HBox del contenido de las letras en stok
     */
    public static HBox recorrerLetra(Map<String, Integer> mapa,int tamanioLetra){
        Font theFont = Font.font("Helvetica", FontWeight.BOLD, tamanioLetra );
        VBox key= new VBox();
        VBox value= new VBox();
        HBox vista= new HBox();
        for(Map.Entry<String,Integer> v: mapa.entrySet()){
            
            Label L1= new Label(v.getKey());
            Label L2= new Label(String.valueOf(v.getValue()));
            L1.setFont(theFont); L1.setTextFill(Color.web("#FFFFFF"));
            L2.setFont(theFont); L2.setTextFill(Color.web("#FFFFFF"));
            key.getChildren().addAll(L1);
            value.getChildren().addAll(L2);  
        }
        key.setAlignment(Pos.CENTER);
        value.setAlignment(Pos.CENTER);
        vista.getChildren().addAll(key,value);
        vista.setSpacing(35);
        vista.setAlignment(Pos.CENTER);

        return vista;
    }
    


    public Pane getRoot() {
        return root;
    }
    
    public void finalizarJuego(){
        pP.finalizarJuegoP();
    }
}
