/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;
import Animacion.EscenaBienvenida;
import RECURSOS.CONSTANTES;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
/**
 *
 * @author Andrea
 */
public class playPalabras {
    private final Pane root;
    private TextField palabras;
    private final Map<String, Integer> dB;
    public static List<String> palabrasIngresadas;
    private Label tiempo;
    private Label puntaje;
    private boolean terminarJuego=false;
    private double tiempoTranscurrido=30;
    private double puntajeAcumulado=0;
    private HBox stock;
    private inDataName iD;
   
    
    public playPalabras(){
        dB= Datos_Letras.tuplaLetra;
        root=new Pane();
        palabrasIngresadas= new ArrayList<>();
        crearFondo();
        tituloLetras();
        mostrarTexto();
        puntaje();
        new Thread( new correrTiempo()).start(); 
        actualizarMapa(); 
    }
    
    public final void crearFondo(){
        ImageView image=new ImageView();
        HBox time= new HBox();
        try {
            Image img= new Image(new FileInputStream(CONSTANTES.PATH_FILES+"fondo_btn.jpg"));
            image.setImage(img);
            image.setFitHeight(700);
            image.setFitWidth(1000);
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        Label lt = new Label("Tiempo: ");
        Inicial.tipoLetra(lt);
        tiempo = new Label(String.valueOf(tiempoTranscurrido));
        Inicial.tipoLetra(tiempo);
        time.getChildren().addAll(lt,tiempo);
        time.setSpacing(5);
        root.getChildren().addAll(image,time);
    }
    
        
    public final void puntaje(){
        HBox score= new HBox();
        Label p= new Label("SCORE: ");
        puntaje= new Label(String.valueOf(puntajeAcumulado));
        Inicial.tipoLetra(puntaje); Inicial.tipoLetra(p);
        score.getChildren().addAll(p,puntaje);
        score.setSpacing(8);
        score.setLayoutX(700);
        root.getChildren().add(score);  
    }
    
    public final void mostrarTexto(){
        VBox contenedora= new VBox();
        Label in= new Label("INGRESE PALABRA: ");
        palabras= new TextField();
        Inicial.tipoLetra(in);
        palabras.setPrefSize(90, 55);
        Inicial.tipoLetra(palabras);
        contenedora.getChildren().addAll(in,palabras);
        contenedora.setSpacing(15);
        contenedora.setLayoutX(400);
        contenedora.setLayoutY(200);
        palabras.setOnKeyPressed((e)->{
            switch(e.getCode()){
                case ENTER:
                    if(autenticacionText(palabras.getText())){
                        actualizarMapa();
                        palabras.clear();
                        palabrasIngresadas.add(palabras.getText());
                        puntaje.setText(String.valueOf(puntajeAcumulado));
                    }
                    break;
            }
        });
        root.getChildren().add(contenedora);
    }

    public final void tituloLetras(){
        Label L1=new Label("LETRAS");
        Label L2=new Label("STOCK");
        Inicial.tipoLetra(L1);
        Inicial.tipoLetra(L2);
        HBox titulo= new HBox();
        titulo.getChildren().addAll(L1,L2);
        titulo.setSpacing(35);
        titulo.setAlignment(Pos.CENTER);
        titulo.setLayoutX(100);
        titulo.setLayoutY(30);
        root.getChildren().add(titulo); 
    }

    /**
     * Metedo que recibe un String(letra) y verifica si cada letra esta en el stock
     * y ademas verifica si la palabra existe en el archivo de palabras registradas
     * @param s palabra ingresada por leclado
     * @return 
     */
    public boolean autenticacionText(String s){
        boolean estado=false;
        String[] palabra= s.split("");
        int num=0;
        for(String p:palabra){
            for(Map.Entry<String,Integer> m:dB.entrySet()){
                if(p.equals(m.getKey()) && m.getValue()>0){
                    num++;
                }
            }
        }
        if(num==palabra.length && leerArchivo(s)){
            estado=true;
            for(String p:palabra){
                for(Map.Entry<String,Integer> m:dB.entrySet()){
                    if(p.equals(m.getKey()) && m.getValue()>0){
                        sumarPuntaje(p);
                        dB.put(m.getKey(), (int)(m.getValue())-1);   
                    }
                }
            }
        }
        return estado; 
    }
    
    /**
     * Metodo actualizarMapa, el cual me actualiza la informacion de las letras 
     * que estan en stock
     */
    public final void actualizarMapa(){
        if(root.getChildren().contains(stock)){
            root.getChildren().remove(stock);
        }
        stock=Datos_Letras.recorrerLetra(dB,17);
        stock.setLayoutX(150);
        stock.setLayoutY(60);
        root.getChildren().add(stock);
    }
    
    private class correrTiempo implements Runnable{
        public void run(){
            while(!terminarJuego){
                tiempoTranscurrido-=0.5;  
                Platform.runLater(()->{  //Me lo manda al hilo principal 
                    tiempo.setText(String.valueOf(tiempoTranscurrido));
                });
                if(tiempoTranscurrido==0){
                    terminarJuego=true;
                    iD= new inDataName(puntajeAcumulado);
                    EscenaBienvenida.sc.setRoot(iD.getRoot());
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    System.out.println(ex.getMessage());
                }     
            }  
        }
    }
    
    /**
     * Metodo que me lee el archivo espanol,csv
     * @param palabra que se ingresa por teclado la verifica en el archivo espanol.csv
     * @return 
     */
    public boolean leerArchivo(String palabra){
        boolean estado=false;
        try (BufferedReader br=new BufferedReader(new FileReader(CONSTANTES.PATH_FILES+"espanol.csv"))){
            //BufferedReader br=new BufferedReader(new FileReader(CONSTANTES.PATH_FILES+"espanol.csv"));
            String linea;
            while((linea=br.readLine())!=null){
                String dato= linea.trim();
                if(palabra.equals(dato)){
                    estado=true;
                }
            }
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }return estado;
    }

    /**
     * Metodo que me suma el puntaje de acuerdo a la letra pasada por parametro
     * @param s, letra pasada por parametro para acumular el puntaje
     */
    public void sumarPuntaje(String s){
        if(s.equals("a") || s.equals("e") || s.equals("o") || s.equals("i") ||s.equals("s") ||s.equals("n") ||
                s.equals("n") ||s.equals("r") ||s.equals("u") ||s.equals("t")){
            puntajeAcumulado+=1;
        }else if(s.equals("d") ||s.equals("g") ){
            puntajeAcumulado+=2;
        }else if(s.equals("b") ||s.equals("m") ||s.equals("p")){
            puntajeAcumulado+=3;
        }else if(s.equals("k") ||s.equals("f") ||s.equals("v") ||s.equals("y")){
            puntajeAcumulado+=4;
        }else if(s.equals("c") ||s.equals("h") ||s.equals("q")){
            puntajeAcumulado+=5;
        }else if(s.equals("j") ||s.equals("l") ||s.equals("ñ") ||s.equals("x")){
            puntajeAcumulado+=8;
        }else if(s.equals("z") ||s.equals("w")){
            puntajeAcumulado+=10;
        }
    }
    
    public Pane getRoot() {
        return root;
    }
    
    public void finalizarJuegoP(){
        terminarJuego=true;
    }

    public double getPuntajeAcumulado() {
        return puntajeAcumulado;
    }
    
    
    
}
