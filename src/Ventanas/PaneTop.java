/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import Animacion.EscenaBienvenida;
import Score.Scores;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 *
 * @author GaryBarzola
 */
public class PaneTop implements Serializable  {
    private final BorderPane root;
    private static Button atras;
    private final Pane gamePane;
    public  static Set<Scores> puntajes = new TreeSet<>();
    static final long serialVersionUID = 1L;
    public PaneTop() {
        gamePane= new Pane();
        root = new BorderPane();
        root.setTop(btn_Atras("Atras"));
        root.setCenter(pantalla());
        mostrarTop();
    }
    
    public void anadirScore(String n, double p){
        puntajes.add(new Scores(n,p));
        
    }
        

    public final Pane pantalla(){
        
        try {
            Image image = new Image(new FileInputStream(RECURSOS.CONSTANTES.PATH_IMAGES+"FondoTopp.jpg"));
            ImageView imagen1 = new ImageView(image);
            imagen1.setFitHeight(700);
            imagen1.setFitWidth(1000);
            gamePane.getChildren().add(imagen1);
        } catch (FileNotFoundException ex) {
            System.out.println("Imagen de fondo no encontrada");
        }
        return gamePane;
    }
           
    public final static Button btn_Atras(String nombre){
        atras= new Button(nombre);
        Font theFont = Font.font("Helvetica", FontWeight.BOLD, 19 );
        atras.setFont(theFont);
        atras.setAlignment(Pos.CENTER);
        atras.setPrefSize(130, 70);
        atras.setTextFill(Color.web("#FFFFFF"));
        try {
            Image img = new Image(new FileInputStream(RECURSOS.CONSTANTES.PATH_IMAGES+"Sangre.png"));
            BackgroundImage image = new BackgroundImage(img, BackgroundRepeat.NO_REPEAT,BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(160,95,false,false,false,false));
            Background background = new Background(image);
            atras.setBackground(background);
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        atras.setOnAction(e->{
            EscenaBienvenida.sc.setRoot((new Inicial()).getRoot());
        });
        return atras;
    }
    
   
    public void CargarTop(){
        VBox nombres = new VBox();
        VBox puntajes_top= new VBox();
        HBox conjunto = new HBox();
        
        for ( Scores p: puntajes){
            System.out.println(p);
            String nombre=p.getNombre();
            double puntaje = p.getPuntaje();
            
            Label nombre_jugador = new Label(nombre);
            Label puntaje_jugador = new Label(String.valueOf(puntaje));
            Inicial.tipoLetra(nombre_jugador);
            Inicial.tipoLetra(puntaje_jugador);
            
            nombres.getChildren().add(nombre_jugador);
            puntajes_top.getChildren().add(puntaje_jugador);
        }
        conjunto.getChildren().addAll(nombres,puntajes_top);
        conjunto.setLayoutX(400);
        conjunto.setSpacing(50);
        
        gamePane.getChildren().addAll(conjunto);
    }
    
    
    public BorderPane getRoot() {
        return root;
    }
    
    /**
     * Metodo que me crea un archivo binario donde se guardan las puntuaciones top
     * @param puntaje, coleccion con los puntajes actuales
     */
    public static void crearDataTop(Set<Scores> puntaje){
        try{
            try (FileOutputStream file = new FileOutputStream("src/RECURSOS/DataTop");
                    ObjectOutputStream obj = new ObjectOutputStream(file)) {
                obj.writeObject(puntaje);
            }
        }catch (IOException e){
            System.out.println("El archivo no se encontro 1");
        }
    }
    
    /**
     * Metodo que me abre un archivo serializado para obtener los puntajes top.
     * @param s
     */
    public static void leerDataTop(Scores s){
        if(!(new File("src/RECURSOS/DataTop")).exists()){
            crearDataTop(puntajes);
        }
        try{
            FileInputStream file = new FileInputStream("src/RECURSOS/DataTop");
            try (ObjectInputStream ob = new ObjectInputStream(file)) {
                puntajes = (TreeSet<Scores>) ob.readObject();
                puntajes.add(s);
                crearDataTop(puntajes);
            }
            
        }catch (IOException e){
            System.out.println("El archivo no se encontro 2");
        }catch(ClassNotFoundException e){
            System.out.println("Error al deserializar");
        }
        
    }
    
    /**
     * Metodo que me muestra los tops en el apartado de puntajes
     */
    public final void mostrarTop(){
        VBox vista=new VBox();
        int veces=10;
        try{
            FileInputStream file = new FileInputStream("src/RECURSOS/DataTop");
            try (ObjectInputStream ob = new ObjectInputStream(file)) {
                puntajes = (TreeSet<Scores>) ob.readObject();
            }
            }catch (IOException e){
            System.out.println("El archivo no se encontro 2");
        }catch(ClassNotFoundException e){
            System.out.println("Error al deserializar");
        }
        Iterator<Scores> i=puntajes.iterator();
        while(veces>0 && i.hasNext()){
            Label top= new Label(((Scores)(i.next())).toString());
            Inicial.tipoLetra(top);
            top.setTextFill(Color.web("#E53935"));
            top.setAlignment(Pos.CENTER);
            vista.getChildren().add(top);
            veces--;
        }
        vista.setLayoutX(410);
        vista.setLayoutY(15);
        gamePane.getChildren().add(vista);
    }
    
    
}
