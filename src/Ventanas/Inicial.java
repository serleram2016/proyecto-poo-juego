/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import Animacion.EscenaBienvenida;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 *
 * @author GaryBarzola
 */
public class Inicial {
    private final Pane root;
    private VBox opciones;
    private PaneJuego pg;
    
    public Inicial() {
        root= new Pane();
        crearPantallaInicial();
        opciones();
        
    }
    
    /**
     * Esta es la seccion donde aparecen los globos. La pantalla Inicial.
     */
    public final void crearPantallaInicial() {
        try {
            Image image = new Image(new FileInputStream(RECURSOS.CONSTANTES.PATH_IMAGES+"Fondo_Inicio.jpg"));
            ImageView imagen1 = new ImageView(image);
            imagen1.setFitHeight(700);
            imagen1.setFitWidth(1000);
            root.getChildren().add(imagen1);
        } catch (FileNotFoundException ex) {
            System.out.println("Imagen de fondo no encontrada");
        }
    }
    
    /**
     * Metodo que me crear el boton inicio
     * @return el boton inicio
     */
    public Button botonInicio(){
        Button iniciar = new Button ("Iniciar");
        iniciar.setAlignment(Pos.CENTER);
        iniciar.setPrefSize(130, 70);
        tipoLetra(iniciar);
        fondoBotones(iniciar);
        iniciar.setOnAction((e) -> {
            pg= new PaneJuego();
            EscenaBienvenida.sc.setRoot(pg.getRoot());
        });
        return iniciar;
    }
    
    /**
     * Metodo que me crear el boton salir
     * @return el boton salir
     */
    public Button botonSalir(){
        Button salir = new Button ("Salir");
        salir.setAlignment(Pos.CENTER);
        salir.setPrefSize(130, 70);  
        tipoLetra(salir);
        fondoBotones(salir);
        salir.setOnAction((e)-> Platform.exit());
        return salir; 
    }
    
    /**
     * Metodo que me crear el boton puntaje
     * @return el boton puntaje
     */
    public Button botonPuntaje(){
        Button puntos = new Button ("Puntaje");
        puntos.setAlignment(Pos.CENTER);   
        puntos.setPrefSize(130, 70);
        tipoLetra(puntos);
        fondoBotones(puntos);
        puntos.setOnAction((e)->{
            EscenaBienvenida.sc.setRoot((new PaneTop()).getRoot());
            PaneTop s = new PaneTop();
        });
        return puntos;
    }
    
    public final void opciones(){
        opciones = new VBox();   
        opciones.getChildren().addAll(botonInicio(),botonPuntaje(),botonSalir());
        opciones.setAlignment(Pos.CENTER);
        opciones.setSpacing(70);
        opciones.setLayoutX(430);
        opciones.setLayoutY(170);
        root.getChildren().add(opciones);
    }
    
    
    public Pane getRoot(){
        return root;
    }
    
    /**
     * Metodo que aniade un fondo de sangre a cualquier boton
     * @param btn, boton al que se le aniade el fondo
     */
    public static void fondoBotones(Button btn){
        try {
            Image img = new Image(new FileInputStream(RECURSOS.CONSTANTES.PATH_IMAGES+"Sangre.png"));
            BackgroundImage image = new BackgroundImage(img, BackgroundRepeat.NO_REPEAT,BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(180,100,false,false,false,false));
            Background background = new Background(image);
            btn.setBackground(background);
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        
    }
    
    public void finalizarJuego(){
        pg.terminarJuego=true;
    }
    
    /**
     * Metodo usado para cambiar el formato de letra.
     * @param obj, dato que recibe puede ser: button, label o textField
     */
    public static void tipoLetra(Object obj){
        if(obj instanceof Button){
            Font theFont = Font.font("Helvetica", FontWeight.BOLD, 19 );
            ((Button)obj).setFont(theFont);
            ((Button)obj).setTextFill(Color.web("#FFFFFF"));  
        }
        else if(obj instanceof Label){
            Font theFont = Font.font("Helvetica", FontWeight.BOLD, 19 );
            ((Label)obj).setFont(theFont);
            ((Label)obj).setTextFill(Color.web("#FFFFFF"));
        }else if(obj instanceof TextField){
            Font theFont = Font.font("Helvetica", FontWeight.BOLD, 19 );
            ((TextField)obj).setFont(theFont);
        }
    }
}

