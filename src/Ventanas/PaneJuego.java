/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import Animacion.EscenaBienvenida;
import RECURSOS.CONSTANTES;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
/**
 *
 * @author GaryBarzola
 */
public class PaneJuego {
    private Pane root;
    private HBox vista;
    private Label marcador;
    private Label tiempo;
    private TextField inLetras;
    public  static boolean terminarJuego=false;
    private double tiempoTranscurrido=60;
    private int globos_rebentados=0;
    private Clip sonido;
    private Clip sonidoG;
    
    public static Map<String,Integer> letrasPantalla;
    public static Map<String,Integer> letrasPresionadas;
            
    private Random random=new Random();
    private Datos_Letras dL;
    
    private List<Globo1> globos;
    
    public PaneJuego() {
        terminarJuego=false;
        root= new Pane();
        letrasPantalla= new TreeMap<>();
        letrasPresionadas= new TreeMap<>();
        globos= new ArrayList();
        pantalla(); 
        agregarTiempo();
        ingresarLetra();
        
        new Thread( new correrTiempo()).start(); 
        new Thread(new Globo()).start();
        try {
            sonido = AudioSystem.getClip();
            sonido.open(AudioSystem.getAudioInputStream(new File(CONSTANTES.PATH_SOUND+"soundP.wav")));
            sonido.start();
        } catch (IOException | LineUnavailableException | UnsupportedAudioFileException ex) {
            System.out.println("" + ex);
        }
    }
    
    public final void pantalla(){
        try {
            Image image = new Image(new FileInputStream(CONSTANTES.PATH_IMAGES+"Fondo_btn.jpg"));
            ImageView imagen1 = new ImageView(image);
            imagen1.setFitHeight(700);
            imagen1.setFitWidth(1000);
            root.getChildren().add(imagen1);
        } catch (FileNotFoundException ex) {
            System.out.println("Imagen de fondo no encontrada");
        }
    }
    
    public final void agregarTiempo(){
        vista = new HBox();
        HBox time= new HBox();
        HBox score= new HBox();
        Font theFont = Font.font("Helvetica", FontWeight.BOLD, 24 );
        Label lt = new Label("Tiempo: ");
        Label lm = new Label("Globos: ");
        lt.setFont(theFont); lm.setFont(theFont);
        lt.setTextFill(Color.web("#FFFFFF")); lm.setTextFill(Color.web("#FFFFFF"));
        tiempo = new Label(String.valueOf(tiempoTranscurrido)); marcador = new Label(String.valueOf(globos_rebentados));
        tiempo.setFont(theFont); marcador.setFont(theFont);
        tiempo.setTextFill(Color.web("#FFFFFF")); marcador.setTextFill(Color.web("#FFFFFF"));
        time.getChildren().addAll(lt,tiempo); score.getChildren().addAll(lm,marcador);
        vista.getChildren().addAll(time,score);
        vista.setSpacing(670);
        root.getChildren().add(vista);
    }

    /**
     * Metodo que recibe una lista de las palabras de que contiene un globo y me 
     * las aniade al mapa pantalla
     * @param letras
     */
    public void llenarLetrasPantalla(List<String> letras){ //Metodo que me aniade datos a al mapa de palabras
        letras.forEach((l) -> {
            int num=0;
            if(letrasPantalla.containsKey(l)){
                num+=letrasPantalla.get(l);
            }
            for(String le:letras){
                if(l.equals(le)){
                    num++;
                }
            }
            letrasPantalla.put(l, num);
        });
    }
    
    /**
     * Metodo que recibe una lista de las palabras de que contiene un globo y me 
     * las elimina del mapa pantalla
     * @param letras
     */
    public void eliminarLetrasPantalla(List<String> letras){
        for(String l: letras){
            if(letrasPantalla.containsKey(l)){
                int num=letrasPantalla.get(l)-1;
                letrasPantalla.put(l, num);
                if(num==0){
                    letrasPantalla.remove(l);
                }
            }
        }
    }
    
    /**
     * Metodo que recibe la letra y si la letra se encuentra en la pantalla
     * me la aniade al mapa de letrasPresionadas
     * @param l
     */
    public void llenarLetrasPresionadas(String l){//La letra que ingreso se agrega al map y se elimana de la pantalla
        if(letrasPantalla.containsKey(l)){
            if(letrasPresionadas.containsKey(l)){
                int num=letrasPresionadas.get(l)+letrasPantalla.get(l);
                letrasPresionadas.put(l, num);
                letrasPantalla.remove(l);
            }else{
                letrasPresionadas.put(l, letrasPantalla.get(l));
                letrasPantalla.remove(l);//Al presionarla me la elimina de la pantalla(Mapa)
            }
        }
    }
    
    /**
     * Metodo que me crea un textFiel para el ingreso de letras por pantalla
     * 
     */
    public final void ingresarLetra(){
        inLetras=new TextField();
        inLetras.setPrefSize(40, 30);
        inLetras.setLayoutY(650);
        root.getChildren().add(inLetras);
        inLetras.setOnKeyPressed((e)->{
            String dato=String.valueOf(e.getCode()).toLowerCase();
            if(letrasPantalla.containsKey(dato)){
                inLetras.clear();
                recorrerGlobos(dato);
                llenarLetrasPresionadas(dato);
            }else{
                inLetras.clear();
                
            }
        });
    }
    
    /**
     * Metodo que verifica los globos en pantalla y actualiza su contenido(letras)
     * eliminando la letra que recibe por parametro s.
     * @param s 
     */
    public synchronized void recorrerGlobos(String s){
        List<Globo1> globosDead=new ArrayList();
        try {
            globos.forEach((g) -> {
                String dato=actualizarLetrasGlobo(g.getWords(),s,g.getColores().get(g.n));
                if(!dato.equals("")){
                    g.setLetras(dato);
                    g.setLa(dato);
                }else{
                    sonidoRevientaGlobo();
                    globosDead.add(g);
                    root.getChildren().remove(g);
                    globos_rebentados++;
                    marcador.setText(String.valueOf(globos_rebentados));
                }
            });
        }catch(ConcurrentModificationException ex){
            System.out.println(ex.getMessage());
        }
        for(Globo1 gd:globosDead){
            for(int i=0; i<globos.size();i++){
                if(gd.equals(globos.get(i))){
                    globos.remove(globos.get(i));
                }
            }
        }
    }
    
    public void sonidoRevientaGlobo(){
        try {
            sonidoG = AudioSystem.getClip();
            sonidoG.open(AudioSystem.getAudioInputStream(new File(CONSTANTES.PATH_SOUND+"rglobo.wav")));
            sonidoG.start();
        } catch (IOException | LineUnavailableException | UnsupportedAudioFileException ex) {
            System.out.println("" + ex);
        }
    }
    
    /**
     * Metodo que me crea un globo y lo aniade al root
     * @return 
     */
    private void añadirGlobo(){  //Me crea un globo en el pane
        double y=698;
        Globo1 g = new Globo1();
        globos.add(g);
        root.getChildren().add(g);
        g.fijarPosicionX(random.nextDouble()*(700));
        new Thread(new subida(g,y)).start();  
        llenarLetrasPantalla(g.getWords());
    }
    
    /**
     * Metodo que recibe una lista de las palabras de un globo
     * y la letra ingresada por teclado y, me devuelve el nuevo
     * texto para el globo.
     * @param l lista de letras del globo
     * @param s letra que paso por teclado
     * @param c color del globo 
     * @return 
     */
    public String actualizarLetrasGlobo(List<String> l, String s, String c){
        String letra="";
        for(int i=0; i<l.size();i++){
            if(!((l.get(i)).toLowerCase()).equals(s)){
                if(!c.equals("Amarillo")){
                    letra+=l.get(i).toLowerCase();
                }
                else{
                    letra+=l.get(i).toUpperCase();
                }
            }
        }
        return letra;
    }
    
    /**
     * Hilo correrTiempo, el cual me aumenta disminuye el tiempo que me queda en pantalla
     * @return 
     */
    private class correrTiempo implements Runnable{ //Hace que el tiempo corra
        @Override
        public void run(){
            while(!terminarJuego){
                tiempoTranscurrido-=1;  
                Platform.runLater(()->{  //Me lo manda al hilo principal 
                    tiempo.setText(String.valueOf(tiempoTranscurrido));
                });
                if(tiempoTranscurrido==0){
                    terminarJuego=true;
                    sonido.close();
                    dL= new Datos_Letras();
                    EscenaBienvenida.sc.setRoot(dL.getRoot());
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    System.out.println(ex.getMessage());
                }
                
            }
            
        }
    }
    
    /**
     * Hilo Globo, el cual me crea varios globos mientras el tiempo no acabe
     * @return 
     */
    private class Globo implements Runnable{ 
        
        @Override
        public void run(){
            while(!terminarJuego){
                Platform.runLater(()->{  //Me lo manda al hilo principal 
                    añadirGlobo();
                });
                if(tiempoTranscurrido==0){
                    terminarJuego=true;
                }
                try{
                    Thread.sleep(800);
                } catch (InterruptedException ex) {
                    System.out.println(ex.getMessage());
                }
                
            } 
        }
    }
    
    /**
     * Hilo subida, el cual es el encargado de editar la posicion de cada globo 
     * permitiendo que el globo suba
     * @return 
     */
    private class subida implements Runnable{  //Me edita la posicion de cada globo.
        final Globo1 g;
        private double y;
        
        public subida(Globo1 g, double y){
            this.g=g;
            this.y=y;
        }
        
        @Override
        public void run(){
            while(!terminarJuego && y!=-61){
                Platform.runLater(()->{  //Me lo manda al hilo principal 
                    g.fijarPosicionY(y);
                });
                if(y<=-60){
                    globos.remove(g);
                    eliminarLetrasPantalla(g.getWords());
                    Platform.runLater(()-> {root.getChildren().remove(g);});
                }
                if(tiempoTranscurrido==0){
                    terminarJuego=true;
                }
                
                try{
                    y--;
                    Thread.sleep(4);
                } catch (InterruptedException ex) {
                    System.out.println(ex.getMessage());
                }
                
            } 
        }
    }
      
    public void finalizarJuego(){
        terminarJuego=true;
        dL.finalizarJuego();
    } 

    public Pane getRoot() {
        return root;
    }
    
}
