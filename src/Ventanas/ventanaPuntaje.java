/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import Animacion.EscenaBienvenida;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

/**
 *
 * @author Gary_Barzola
 */
public class ventanaPuntaje {
    private final BorderPane root;
    private final StackPane resultado;
    private final String nombre;
    private final double puntaje;
    private Button atras; 
    //private static PrintStream consolePrint;
    private final HBox hh;
    private Button compartir;
    
    public ventanaPuntaje(String n, double p){
        nombre=n;
        puntaje=p;
        root=new BorderPane();
        resultado= new StackPane();
        hh= new HBox();
        hh.getChildren().add(PaneTop.btn_Atras("MENU"));
        hh.setSpacing(20);
        root.setCenter(pantalla());
        root.setTop(hh);
        mostrarResultado();
        botoncompartir();
    }

    /**
     * Metodo que me aniade el boton compartir para postear un resultado a twitter
     */
    public final void botoncompartir(){
        compartir = new Button(" Compartir");
        Font theFont = Font.font("Helvetica", FontWeight.BOLD, 19 );
        compartir.setFont(theFont);
        compartir.setAlignment(Pos.CENTER);
        compartir.setPrefSize(130, 70);
        compartir.setTextFill(Color.web("#000000"));
        try {
            Image img = new Image(new FileInputStream(RECURSOS.CONSTANTES.PATH_IMAGES+"Twitter.png"));
            BackgroundImage image = new BackgroundImage(img, BackgroundRepeat.NO_REPEAT,BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, new BackgroundSize(160,95,false,false,false,false));
            Background background = new Background(image);
            compartir.setBackground(background);
        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        hh.getChildren().add(compartir); 
        compartir.setOnAction(ee->{
            String mensaje= " Enhorabuena ! El jugador " + this.nombre + " ha obtenido un puntaje de: " + this.puntaje;
            try{
                ConfigurationBuilder cb = new ConfigurationBuilder();
                cb.setDebugEnabled(true)
                .setOAuthConsumerKey("slC5dpnSymFDnSMPXZrIPIbv7")
                .setOAuthConsumerSecret("2HEWUQaDeSocDWRNaz7n3SQseZPKT8BKt2aj6x4HPv6wSCjNfm")
                .setOAuthAccessToken("1168373069888544768-vKe6Yg7WTdakPeD92DmFua3p3IHCXu")
                .setOAuthAccessTokenSecret("mnsRoPQJ00iJy1CCPWXm9MEtfSo0pn1IWoHrtxxyNJP8X");
                TwitterFactory tf = new TwitterFactory(cb.build());
                Twitter twitter = tf.getInstance();
                Status stat= twitter.updateStatus(mensaje);
                System.out.println("Tweet posteado exitosamente");
                hh.getChildren().remove(compartir);
               }catch(TwitterException ex ){
                   System.out.println("Ha ocurrido un error al momento de compartir el resultado");           
               }catch(Exception ex ){
                   System.err.println("Ha ocurrido un error al procesar los datos y compartir el resutlado"); }
        });
         
   } 
    
    /**
     * Metodo que me crear la imagen para el fondo de los puntajes
     * @return la pantalla de fondo de la ventana
     */
    public final StackPane pantalla(){

        try {
            Image image = new Image(new FileInputStream(RECURSOS.CONSTANTES.PATH_IMAGES+"fondo_btn.jpg"));
            ImageView imagen1 = new ImageView(image);
            imagen1.setFitHeight(700);
            imagen1.setFitWidth(1000);
            resultado.getChildren().add(imagen1);
        } catch (FileNotFoundException ex) {
            System.out.println("Imagen de fondo no encontrada");
        }
        return resultado;
    }
    
    public final void mostrarResultado(){
        Font theFont = Font.font("Helvetica", FontWeight.BOLD, 30 );
        Label n= new Label(this.nombre); n.setFont(theFont); n.setTextFill(Color.web("#FFFFFF"));
        Label p= new Label(String.valueOf(this.puntaje)+" Puntos"); p.setFont(theFont); p.setTextFill(Color.web("#FFFFFF"));
        Label x= new Label("--->"); Inicial.tipoLetra(x); x.setFont(theFont); x.setTextFill(Color.web("FFFFFF"));
        HBox vista= new HBox();
        vista.getChildren().addAll(n,x,p);
        vista.setSpacing(15);
        vista.setAlignment(Pos.CENTER);
        resultado.getChildren().add(vista);
        resultado.setLayoutX(400);
        resultado.setLayoutY(400);
    }

    public BorderPane getRoot() {
        return root;
    }

}
