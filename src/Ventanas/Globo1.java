/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

/**
 *
 * @author Andrea
 */


public class Globo1 extends Pane{
    private final List<String> colores;
    public List<String> letras;
    public Label la;
    private final Font theFont = Font.font("Helvetica", FontWeight.BOLD, 27 );
    int n=(int)(Math.random()*3);
    
    public Globo1(){
        super();
        la=new Label();
        letras= new ArrayList<>();
        colores = new ArrayList();
        llenarColores();
        crearGlobo(n); 
        letrasGlobo(n);
    }
    
    public final void llenarColores(){
        colores.add("Amarillo");
        colores.add("Rojo");
        colores.add("Verde");
    } 
    
    public final void crearGlobo(int n){
        ImageView globito=new ImageView();   
        try {
            Image img = new Image(new FileInputStream(RECURSOS.CONSTANTES.PATH_IMAGES+"globo"+colores.get(n)+".png"));
            globito.setImage(img);
            globito.setFitHeight(160);
            globito.setFitWidth(115);

        } catch (FileNotFoundException ex) {
            System.out.println(ex.getMessage());
        }
        this.getChildren().addAll(globito);
        
    }
    
    public final void letrasGlobo(int n){
        la=new Label(crear_Letras(n));
        la.setPrefWidth(115);
        la.setAlignment(Pos.CENTER);
        la.setFont(theFont);
        la.setTextFill(Color.web("#000000"));
        la.setLayoutY(17);
        this.getChildren().add(la);
    }
    
    
    public String crear_Letras(int n){ //Me crea las letras para cada globo depediendo el color(n)
        int numero_letras=(int)(Math.random()*(2))+2;
        String letra="";
        switch(n){
            case 0:
                String constante=String.valueOf(generarLetra());
                letras.add(constante); //globo amarillo
                letra=constante.toUpperCase();
                break;
            case 1:
                for(int i=0;i<numero_letras;i++ ){
                    String constante2=String.valueOf(generarLetra());
                    letras.add(constante2); 
                    letra+=constante2;
                }
                break;
            case 2:
                String constante3=String.valueOf(generarLetra());
                letras.add(constante3);
                letra=constante3;
                break;
        }return letra;
    }
    
    public char generarLetra(){
        int numAleatorio = (int)Math.floor(Math.random()*(26)+97);
	//para transformar los números en letras según ACSII
	return (char)numAleatorio;
    }

    
    
    public void fijarPosicionY(double y){
        this.setLayoutY(y);
    }
    
    public void fijarPosicionX(double x){
        this.setLayoutX(x);
    }
    
    public double getPosicionX(){
        return this.getLayoutX();
    }
    
    public double getPosicionY(){
        return this.getLayoutY();
    }

    public List<String> getWords() {
        return letras;
    }

    public void setLetras(String letras) {
        this.letras.clear(); 
        String[] l=letras.split("");
        for(String s: l){
            this.letras.add(s);
        }
    }
    
    
    public Label getLa() {
        return la;
    }

    public void setLa(String value) {
        this.la.setText(value);
    }

    public List<String> getColores() {
        return colores;
    }
    
    
    
}