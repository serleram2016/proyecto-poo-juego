/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Score;

import java.io.Serializable;

/**
 *
 * @author Andrea
 */
public class Scores implements Comparable<Scores> , Serializable{
    public String nombre;
    private double puntaje;
    static final long serialVersionUID = 1L;
    public Scores(String nombre, double puntaje) {
        this.nombre = nombre;
        this.puntaje = puntaje;
    }

    public Scores(String nombre){
        this.nombre = nombre;
    }
    
    public String getNombre() {
        return nombre;
    }

    public double getPuntaje() {
        return puntaje;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }

    @Override
    public int compareTo(Scores s) {
        if(new Double(puntaje).compareTo(s.puntaje )==0){
            return (-1)*((nombre)).compareTo((s.nombre));
        }else{
            return (-1)*(new Double(puntaje).compareTo(s.puntaje ));
        }
    }

    @Override
    public String toString() {
        return nombre + "   ---->   " + puntaje;
    }

    
}
