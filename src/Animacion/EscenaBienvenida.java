/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Animacion;

import Ventanas.Inicial;
import java.io.FileNotFoundException;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author CltControl
 */
public class EscenaBienvenida extends Application{
    Inicial gp;
    public static Scene sc;
    


    
    @Override
    public void start(Stage st) throws FileNotFoundException{
        gp = new Inicial();

        sc = new Scene (gp.getRoot()) ;
        st.setTitle("TyperBallons");
        st.setScene(sc);
        st.show();
        
    }
        
    public static void main(String[] arg){
        launch(arg);
    }
    
    public void stop(){
        gp.finalizarJuego();
    }
    
}
